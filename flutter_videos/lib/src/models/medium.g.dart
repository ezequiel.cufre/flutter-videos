// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'medium.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Medium _$MediumFromJson(Map<String, dynamic> json) {
  return Medium(url: json['url'] as String);
}

Map<String, dynamic> _$MediumToJson(Medium instance) =>
    <String, dynamic>{'url': instance.url};
