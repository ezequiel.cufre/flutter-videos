import 'package:json_annotation/json_annotation.dart';
part 'id.g.dart';

@JsonSerializable()
class IdVideo {
  @JsonKey(name: 'videoId')
  String videoId;

  IdVideo({this.videoId});

  factory IdVideo.fromJson(Map<String, dynamic> json) =>
      _$IdVideoFromJson(json);

  Map<String, dynamic> toJson() => _$IdVideoToJson(this);
}
