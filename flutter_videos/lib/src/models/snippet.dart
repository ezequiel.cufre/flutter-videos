import 'package:flutter_videos/src/models/thumbnail.dart';
import 'package:json_annotation/json_annotation.dart';
part 'snippet.g.dart';

@JsonSerializable()
class Snippet {
  @JsonKey(name: 'title')
  String title;
  @JsonKey(name: 'description')
  String description;
  @JsonKey(name: 'channelTitle')
  String channelTitle;
  @JsonKey(name: 'thumbnails')
  Thumbnail thumbnail;

  Snippet({this.title, this.description, this.channelTitle, this.thumbnail});

  factory Snippet.fromJson(Map<String, dynamic> json) =>
      _$SnippetFromJson(json);

  Map<String, dynamic> toJson() => _$SnippetToJson(this);
}
