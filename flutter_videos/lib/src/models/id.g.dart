// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'id.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

IdVideo _$IdVideoFromJson(Map<String, dynamic> json) {
  return IdVideo(videoId: json['videoId'] as String);
}

Map<String, dynamic> _$IdVideoToJson(IdVideo instance) =>
    <String, dynamic>{'videoId': instance.videoId};
