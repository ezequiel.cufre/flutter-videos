// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'snippet.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Snippet _$SnippetFromJson(Map<String, dynamic> json) {
  return Snippet(
      title: json['title'] as String,
      description: json['description'] as String,
      channelTitle: json['channelTitle'] as String,
      thumbnail: json['thumbnails'] == null
          ? null
          : Thumbnail.fromJson(json['thumbnails'] as Map<String, dynamic>));
}

Map<String, dynamic> _$SnippetToJson(Snippet instance) => <String, dynamic>{
      'title': instance.title,
      'description': instance.description,
      'channelTitle': instance.channelTitle,
      'thumbnails': instance.thumbnail
    };
