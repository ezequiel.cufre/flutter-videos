import 'package:flutter_videos/src/models/medium.dart';
import 'package:json_annotation/json_annotation.dart';
part 'thumbnail.g.dart';

@JsonSerializable()
class Thumbnail {
  @JsonKey(name: 'medium')
  Medium medium;

  Thumbnail({this.medium});

  factory Thumbnail.fromJson(Map<String, dynamic> json) =>
      _$ThumbnailFromJson(json);

  Map<String, dynamic> toJson() => _$ThumbnailToJson(this);
}
