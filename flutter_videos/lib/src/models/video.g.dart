// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'video.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Video _$VideoFromJson(Map<String, dynamic> json) {
  return Video(
      id: json['id'] == null
          ? null
          : IdVideo.fromJson(json['id'] as Map<String, dynamic>),
      snippet: json['snippet'] == null
          ? null
          : Snippet.fromJson(json['snippet'] as Map<String, dynamic>));
}

Map<String, dynamic> _$VideoToJson(Video instance) =>
    <String, dynamic>{'id': instance.id, 'snippet': instance.snippet};
