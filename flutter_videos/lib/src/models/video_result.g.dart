// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'video_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VideoResult _$VideoResultFromJson(Map<String, dynamic> json) {
  return VideoResult(
      items: (json['items'] as List)
          ?.map((e) =>
              e == null ? null : Video.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      nextPageToken: json['nextPageToken'] as String);
}

Map<String, dynamic> _$VideoResultToJson(VideoResult instance) =>
    <String, dynamic>{
      'items': instance.items,
      'nextPageToken': instance.nextPageToken
    };
