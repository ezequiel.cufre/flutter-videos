import 'package:json_annotation/json_annotation.dart';
part 'medium.g.dart';

@JsonSerializable()
class Medium {
  @JsonKey(name: 'url')
  String url;

  Medium({this.url});

  factory Medium.fromJson(Map<String, dynamic> json) => _$MediumFromJson(json);

  Map<String, dynamic> toJson() => _$MediumToJson(this);
}
