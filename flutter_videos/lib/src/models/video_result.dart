import 'package:flutter_videos/src/models/video.dart';

import 'package:json_annotation/json_annotation.dart';
part 'video_result.g.dart';

@JsonSerializable()
class VideoResult {
  @JsonKey(name: 'items')
  List<Video> items;
  @JsonKey(name: 'nextPageToken')
  String nextPageToken;

  VideoResult({this.items, this.nextPageToken});

  factory VideoResult.fromJson(Map<String, dynamic> json) =>
      _$VideoResultFromJson(json);

  Map<String, dynamic> toJson() => _$VideoResultToJson(this);
}
