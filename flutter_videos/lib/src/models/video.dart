import 'package:flutter_videos/src/models/id.dart';

import 'package:flutter_videos/src/models/snippet.dart';
import 'package:flutter_videos/src/models/thumbnail.dart';
import 'package:json_annotation/json_annotation.dart';
part 'video.g.dart';

@JsonSerializable()
class Video {
  @JsonKey(name: 'id')
  IdVideo id;
  @JsonKey(name: 'snippet')
  Snippet snippet;

  Video({this.id, this.snippet});

  factory Video.fromJson(Map<String, dynamic> json) => _$VideoFromJson(json);

  Map<String, dynamic> toJson() => _$VideoToJson(this);

  List<Video> listFromJson(List<dynamic> json) {
    List<Video> videoList = List<Video>();
    for (var item in json) {
      videoList.add(Video.fromJson(item));
    }
    return videoList;
  }
}
