import 'package:flutter/material.dart';
import 'package:flutter_videos/src/widgets/list.videos.dart';

class FavouritesPage extends StatefulWidget {
  @override
  _FavouritesPageState createState() => new _FavouritesPageState();
}

class _FavouritesPageState extends State<FavouritesPage> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: FlutterLogo(
        size: MediaQuery.of(context).size.height * 0.7,
      ),
    );
  }
}
