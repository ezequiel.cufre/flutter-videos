import 'package:flutter/material.dart';
import 'package:flutter_videos/src/screens/Recommendations_page.dart';
import 'package:flutter_videos/src/screens/favourites_page.dart';
import 'package:flutter_videos/src/screens/search_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => new _HomePageState();
}

enum TabItem { recommendations, favourites, search }

class _HomePageState extends State<HomePage> {
  TabItem currentItem = TabItem.recommendations;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      backgroundColor: Color.fromRGBO(240, 240, 240, 1),
      body: WillPopScope(
        onWillPop: () async => false,
        child: _buildBody(),
      ),
      bottomNavigationBar: _buildBottomNavigationBar(),
    );
  }

  Widget _appBar() {
    return AppBar(
      title: Image.asset("assets/images/flutter_logo.png"),
      automaticallyImplyLeading: false,
    );
  }

  Widget _buildBody() {
    switch (currentItem) {
      case TabItem.recommendations:
        return RecommendationsPage();
      case TabItem.favourites:
        return FavouritesPage();
      case TabItem.search:
        return SearchPage();
    }

    return RecommendationsPage();
  }

  Widget _buildBottomNavigationBar() {
    return BottomAppBar(
      child: Padding(
        padding: const EdgeInsets.only(bottom: 4.0),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: _navBarOptions(),
        ),
      ),
    );
  }

  List<Widget> _navBarOptions() {
    List<Widget> list = [
      _buildItem(
        icon: Icon(Icons.explore),
        iconPressed: Icon(
          Icons.explore,
          color: Colors.blue,
        ),
        tabItem: TabItem.recommendations,
        text: "Recomendaciones",
      ),
      _buildItem(
        icon: Icon(Icons.star),
        iconPressed: Icon(
          Icons.star,
          color: Colors.blue,
        ),
        tabItem: TabItem.favourites,
        text: "Favoritos",
      ),
      _buildItem(
        icon: Icon(Icons.search),
        iconPressed: Icon(Icons.search, color: Colors.blue),
        tabItem: TabItem.search,
        text: "Buscar",
      ),
    ];
    list.removeWhere((value) => value == null);
    return list.toList();
  }

  Widget _buildItem(
      {Icon icon, Icon iconPressed, TabItem tabItem, String text}) {
    return Expanded(
      child: GestureDetector(
        child: Container(
          color: _colorBackgroundMatching(item: tabItem),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                _iconMatching(
                  item: tabItem,
                  icon: icon,
                  iconPressed: iconPressed,
                ),
                Text(
                  text,
                  style: TextStyle(
                    color: _colorTextMatching(item: tabItem),
                    fontSize: 10.0,
                    fontFamily: 'Cera Pro Bold',
                  ),
                ),
              ],
            ),
          ),
        ),
        onTap: () => _updateCurrentItem(item: tabItem),
      ),
    );
  }

  _updateCurrentItem({TabItem item}) {
    setState(() {
      currentItem = item;
    });
  }

  Color _colorBackgroundMatching({TabItem item}) {
    return currentItem == item ? Color(0xFFf5f5f5) : Colors.white;
  }

  Icon _iconMatching({TabItem item, Icon icon, Icon iconPressed}) {
    return currentItem == item ? iconPressed : icon;
  }

  Color _colorTextMatching({TabItem item}) {
    return currentItem == item ? Colors.blue : Colors.black87;
  }
}
