import 'package:flutter/material.dart';
import 'package:flutter_videos/src/screens/home.dart';
import 'package:flutter_videos/src/utils/navigations/navigator.dart';

class LanguageSelector extends StatefulWidget {
  @override
  _LanguageSelectorState createState() => new _LanguageSelectorState();
}

class _LanguageSelectorState extends State<LanguageSelector> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _body(),
    );
  }

  Widget _body() {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[_logo(), _selectLanguageText(), _listLanguages()],
      ),
    );
  }

  Widget _logo() {
    return Container(
      color: Colors.white,
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.3,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Image.asset(
            "assets/images/flutter_logo.png",
            scale: 1.5,
          ),
        ],
      ),
    );
  }

  Widget _selectLanguageText() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(15.0),
          child: Text("Select your Language"),
        ),
      ],
    );
  }

  Widget _listLanguages() {
    return Container(
      width: MediaQuery.of(context).size.width * 0.95,
      child: ListView(
        shrinkWrap: true,
        addRepaintBoundaries: true,
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          _listItem("Español"),
          _listItem("English"),
          _listItem("Portuguese"),
        ],
      ),
    );
  }

  Widget _listItem(String language) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0),
      child: GestureDetector(
        onTap: () {
          GeneralNavigator(context, HomePage()).navigate();
        },
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 15.0),
                  child: Text(language),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
