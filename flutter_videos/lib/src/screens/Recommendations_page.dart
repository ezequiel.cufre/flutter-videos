import 'package:flutter/material.dart';
import 'package:flutter_videos/src/models/video.dart';
import 'package:flutter_videos/src/models/video_result.dart';
import 'package:flutter_videos/src/services/youtube_api/videos.dart';
import 'package:url_launcher/url_launcher.dart';

class RecommendationsPage extends StatefulWidget {
  @override
  _RecommendationsPageState createState() => new _RecommendationsPageState();
}

class _RecommendationsPageState extends State<RecommendationsPage> {
  VideoResult _videoResult = VideoResult();
  List<Video> _videoList = List<Video>();

  _fetchVideos(String tokenNextPage) async {
    VideoModule().searchVideo("Flutter widgets", tokenNextPage).then((result) {
      setState(() {
        _videoResult = result;
        _videoList.addAll(result.items);
      });
    });
  }

  @override
  void initState() {
    _fetchVideos(null);
    super.initState();
  }

  _launchURL(String id) async {
    String url = 'https://www.youtube.com/watch?v=$id';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _buildList(),
    );
  }

  Widget _buildList() {
    return ListView.builder(
        shrinkWrap: true,
        addRepaintBoundaries: true,
        itemCount: _videoList.length,
        itemBuilder: (BuildContext context, int index) {
          if (index >= _videoList.length - 2) {
            _fetchVideos(_videoResult.nextPageToken);
          }
          return _buildItem(_videoList[index]);
        });
  }

  Widget _buildItem(Video item) {
    return Card(
      child: ListTile(
        contentPadding: EdgeInsets.symmetric(horizontal: 7.0),
        leading: Image.network(item.snippet.thumbnail.medium.url),
        title: Text(item.snippet.title),
        // subtitle: Text(
        //   item.snippet.description,
        //   overflow: TextOverflow.ellipsis,
        // ),
        onTap: () => _launchURL(item.id.videoId),
      ),
    );
  }
}
