import 'package:http/http.dart' as http;
import 'dart:convert';

class ApiResponse {
  //ApiHandler apiHandler = ApiHandler();

  Future<dynamic> getJson(Uri uri) async {
    var response = await http.get(uri);
    return response;
    // return _parseResponse(response);
  }

  Future<dynamic> postJson(
    Uri uri,
    Map<String, dynamic> header,
    Map<String, dynamic> body,
  ) async {
    var response =
        await http.post(uri, headers: header, body: json.encode(body));

    return response;
    // return _parseResponse(response);
  }

  Future<dynamic> putJson(
    Uri uri,
    Map<String, dynamic> header,
    Map<String, dynamic> body,
  ) async {
    var response =
        await http.put(uri, headers: header, body: json.encode(body));

    return response;
    //return _parseResponse(response);
  }

  // _parseResponse(var response) {
  //   final result = apiHandler.handlerResponse(response);
  //   return json.decode(result.body);
  // }
}
