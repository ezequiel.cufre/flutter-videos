import 'package:flutter_videos/src/common/config.dart';
import 'package:flutter_videos/src/models/video.dart';
import 'package:flutter_videos/src/models/video_result.dart';
import 'package:flutter_videos/src/services/api_response.dart';
import 'dart:convert';

class VideoModule {
  String baseUrl = Config.baseUrl;
  String apiKey = Config.apiKey;
  String modulePath = '/youtube/v3';
  ApiResponse apiResponse = ApiResponse();

  // GET
  searchVideo(String value, String tokenPage) async {
    String path = '/search';
    Map<String, String> _query = {
      'part': 'snippet',
      'q': value,
      'key': apiKey,
      'type': 'video',
      'pageToken': tokenPage == null ? "" : tokenPage,
    };

    var uri = new Uri.https(baseUrl, modulePath + path, _query);
    var response = await apiResponse.getJson(uri);
    var jsonResult = json.decode(utf8.decode(response.bodyBytes));

    return VideoResult(
      items: Video().listFromJson(jsonResult['items']),
      nextPageToken: jsonResult['nextPageToken'],
    );
  }
}
